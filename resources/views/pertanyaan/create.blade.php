@extends('layout.master')

@section('konten-header')
Forum
@endsection

@section('konten')
<div class="row">
    <div class="col-12">
        <div class="card">
            {!! Form::model($pertanyaan, [
            'method' => 'POST',
            'route' => 'pertanyaan.store'
            ])
            !!}

            @include('pertanyaan.form')

            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection