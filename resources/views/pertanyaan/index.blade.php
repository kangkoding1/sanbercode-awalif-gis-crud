@extends('layout.master')

@section('konten-header')
Forum
@endsection

@section('breadcumb')
<div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('pertanyaan.create') }}">Ajukan Pertanyaan</a></li>
    </ol>
</div>
@endsection

@section('konten')

@if($pertanyaanCount == 0)

<div class="callout callout-danger">
    <h5>Oopss!</h5>

    <p>Data masih kosong.</p>
</div>

@else



<div class="row d-flex align-items-stretch">
    @foreach($pertanyaan as $data)
    <div class="col-12 col-sm-3 col-md-3 d-flex align-items-stretch">
        <div class="card bg-dark">
            <div class="card-header text-light border-bottom-0">
                Nama Penanya
            </div>
            <div class="card-body pt-0">
                <div class="row">
                    <div class="col-6">
                        <h2 class="lead"><b>{{ $data->judul }}</b></h2>
                        <p class="text-light text-sm">{{ $data->isi }}</p>
                    </div>
                    <div class="col-6 text-center">
                        <i class="fas fa-question-circle fa-9x mr-5"></i>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="text-center">
                    <a href="{{ route('pertanyaan.show', $data->id) }}" class="btn btn-md bg-info">
                        <i class="fas fa-info"></i> Detail
                    </a>
                    <a href="{{ route('pertanyaan.edit', $data->id) }}" class="btn btn-md bg-primary">
                        <i class="fas fa-pen"></i> Edit
                    </a>
                    {!! Form::open(['method' => 'DELETE', 'route' => ['pertanyaan.destroy', $data->id]]) !!}
                        {!! Form::button('<i class="fas fa-trash"></i> Hapus', ['type' => 'submit', 'class' => 'btn btn-md bg-danger my-5'] ) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>

@endif

@endsection