@extends('layout.master')

@section('konten-header')
Forum
@endsection

@section('konten')
<div class="row">
    <div class="col-12">
        <div class="card">
            {!! Form::model($pertanyaan, [
            'method' => 'PUT',
            'route' => ['pertanyaan.update', $pertanyaan->id]
            ])
            !!}

            @include('pertanyaan.form')

            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection