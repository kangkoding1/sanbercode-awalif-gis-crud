<form role="form">
    <div class="card-body">
        <div class="form-group">
            {!! Form::label('judul', 'Judul') !!}
            {!! Form::text('judul', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('isi', 'Isi') !!}
            {!! Form::textarea('isi', null, ['class' => 'form-control']) !!}
        </div>

    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        {!! Form::button('Simpan', ['type' => 'submit', 'class' => 'btn btn-primary'] ) !!}
    </div>
</form>