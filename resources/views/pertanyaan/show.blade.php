@extends('layout.master')

@section('konten-header')
Forum
@endsection

@section('konten')
<div class="row">
    <div class="col-12">
        <!-- Post -->
        <div class="post">
            <div class="user-block">
                <img class="img-circle img-bordered-sm" src="{{ asset('frontend/img/question.jpg') }}" alt="user image">
                <span class="username">
                    <a href="#">{{ $pertanyaan->judul }}</a>
                </span>
                <span class="description">{{ $pertanyaan->created_at }}</span>
            </div>
            <!-- /.user-block -->
            <p>{{ $pertanyaan->isi }}</p>

            <!--<p>
                <a href="#" class="link-black text-sm mr-5"><i class="far fa-thumbs-up mr-1"></i> Like</a>
                <a href="#" class="link-black text-sm"><i class="far fa-thumbs-down mr-1"></i> Dislike</a>
            </p>-->

            <form class="form-horizontal">
                <div class="input-group input-group-sm mb-0">
                    <input class="form-control form-control-sm" placeholder="Tulis jawaban ... (Sedang pengembangan)">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-success">Kirim</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.post -->
    </div>
</div>
@endsection